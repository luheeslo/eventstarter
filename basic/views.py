# -*- coding: utf-8 -*-
from basic.forms import UserProfileForm
from django.views.generic import FormView
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator


class LoggedInMixin(object):

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoggedInMixin, self).dispatch(*args, **kwargs)


class SignUpView(SuccessMessageMixin, FormView):
    form_class = UserProfileForm
    template_name = 'basic/userprofile_form.html'

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            form.save()
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        return self.render_to_response(self.get_context_data(registered=True))


