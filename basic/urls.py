from django.conf.urls import patterns, url
from models import UserProfile, Event, Guest
from django.views import generic
from basic import views

all_user = {
    'queryset': UserProfile.objects.all()
}

all_event = {
    'queryset': Event.objects.all()
}


all_guest = {
    'queryset': Guest.objects.all()
}

urlpatterns = patterns(
    '',
    url(r'^$', generic.TemplateView.as_view(template_name='basic/index.html'),
        name='index'),
    url(r'^events/$', generic.ListView.as_view(model=Event),
        name='list_events'),
    url(r'^events/(?P<pk>\d+)$', generic.DetailView.as_view(model=Event),
        name='detail_event'),
    url(r'^new/$', views.SignUpView.as_view(),
        name='user_new'),
)
