from django import forms
from basic.models import UserProfile
from django.contrib.auth.models import User


class UserProfileForm(forms.Form):
    first_name = forms.CharField()
    last_name = forms.CharField()
    username = forms.CharField()
    email = forms.CharField(widget=forms.EmailInput())
    password = forms.CharField(widget=forms.PasswordInput())
    date_birth = forms.DateField(input_formats=['%d/%m/%Y'])

    def save(self):
        data = self.cleaned_data
        u = User.objects.create_user(username=data['username'],
                                     password=data['password'],
                                     email=data['email'],
                                     first_name=data['first_name'],
                                     last_name=data['last_name'])

        UserProfile.objects.get_or_create(user=u,
                                          date_birth=data['date_birth'])
