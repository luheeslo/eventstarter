from django.db import models
from django.contrib.auth.models import User


class UserProfile(models.Model):

    user = models.OneToOneField(User)
    date_birth = models.DateField()

    def __unicode__(self):
        return '%s %s' % (self.user.first_name, self.user.last_name)

    class Meta:
        verbose_name_plural = 'Users Profile'


class Event(models.Model):

    name = models.CharField(max_length=120)
    description = models.TextField()
    image = models.ImageField(upload_to='event_images', null=True, blank=True)
    private = models.BooleanField()
    start_time = models.DateTimeField()
    location = models.CharField(max_length=200)
    full_amount = models.DecimalField(max_digits=11, decimal_places=2)
    amount_donated = models.DecimalField(max_digits=11,
                                         decimal_places=2, default=0.0)
    user_profile = models.ForeignKey(UserProfile)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name_plural = 'Events'


class Guest(models.Model):

    name = models.CharField(max_length=80)
    email = models.EmailField(unique=True)
    cod_invited = models.CharField(max_length=50, editable=False, null=True)
    expired = models.DateTimeField(editable=False, blank=True)
    event = models.ForeignKey(Event)

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.cod_invited = '{}{}'.format(self.name, self.id)
        self.expired = self.event.start_time
        super(Guest, self).save(*args, **kwargs)

    class Meta:
        ordering = ['name']
        verbose_name_plural = 'Guests'
