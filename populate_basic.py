# -*- coding: utf-8 -*-
import os
import datetime


def populate():

    user1 = create_user('Carlos', 'Alberto', datetime.date(1978, 10, 2),
                        'carlosalberto@gmail.com', 'carlos12')

    event1 = create_event('Aniversário surpresa de Mário',
                          'Venham para o aniversário surpresa de Mário. \
                          Vai ser divertido.',
                          True,
                          datetime.datetime(2015, 02, 12, 14, 0),
                          'Avenida não sei das quantas, Cidade, Estado',
                          1000,
                          user1)

    create_guest('Maria', 'maria@gmail.com', event1)

    create_guest('Andressa', 'andressa@gmail.com', event1)

    create_guest('Carla', 'carla@gmail.com', event1)

    user2 = create_user('Mario', 'Canuto', datetime.date(1982, 11, 2),
                        'mariocanuto@gmail.com', 'ca12')

    event2 = create_event('Festa de despedida de Raquek',
                          """
                        Festa de despedida de Raquel.Para esse evento dá certo,
                        os convidados tem que fornce uma determinada quantia.
                        Esse doação servirá para comprar as bebidas e a banda.
                          """,
                          True,
                          datetime.datetime(2015, 05, 20, 18, 0),
                          'Avenida não sei das quantas, Cidade, Estado',
                          4000,
                          user2)

    create_guest('Natasha', 'natasha@gmail.com', event2)

    create_guest('Sônia', 'sonia@gmail.com', event2)

    create_guest('Simone', 'simone@gmail.com', event2)

    user3 = create_user('Lais', 'Couto', datetime.date(1982, 11, 2),
                        'laiscout@gmail.com', 'couto12')

    event3 = create_event('Meu chá de bebê',
                          """
                          Pessoal, meu chá de bebê estar bem pertinho, por is-
                          so vocês podem doar
                          uma quantidade significativa para eu poder
                          blablabla blablabla blablabla
                          """,
                          True,
                          datetime.datetime(2015, 10, 10, 14, 0),
                          'Avenida não sei das quantas, Cidade, Estado',
                          4000,
                          user3)

    create_guest('Rocha', 'rocha@gmail.com', event3)

    create_guest('Blade', 'blade@gmail.com', event3)

    create_guest('Kang', 'kang@gmail.com', event3)

    for u in UserProfile.objects.all():
        print "Nome do usuário:{}".format(u)
        for e in Event.objects.filter(user_profile=u):
            print "Nome do Evento:{}".format(e)
            for g in Guest.objects.filter(event=e):
                print "Nome dos convidados:{}".format(g)


def create_user(first_name, last_name, date_birth, email, password):

    u = User.objects.create_user(username=first_name.lower(),
                                 password=password, email=email,
                                 first_name=first_name, last_name=last_name)

    up = UserProfile.objects.get_or_create(user=u, date_birth=date_birth)[0]
    return up


def create_event(name, description, private, start_time, location, full_amount,
                 user):
    e = Event.objects.get_or_create(name=name, description=description,
                                    private=private, start_time=start_time,
                                    location=location, full_amount=full_amount,
                                    user_profile=user)[0]
    return e


def create_guest(name, email, event):
    g = Guest.objects.get_or_create(name=name, email=email, event=event)[0]
    return g

if __name__ == '__main__':
    print "Starting basic population script..."
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'EventStarter.settings')
    from basic.models import Event, UserProfile, Guest
    from django.contrib.auth.models import User
    populate()
