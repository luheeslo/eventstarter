# -*- coding: utf-8 -*-
from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import unittest


class CriarEventoTest(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_login_e_criacao_de_evento(self):

        # Carlos se cadastrou no sistema e agora deseja criar um evento
        # Ele acessa a URL do site
        self.browser.get('http://localhost:8000/basic')

        # Ao carregar a home, ele ver o link 'Log In' no cabeçario
        # da página
        a_element = self.browser.find_element(By.XPATH, '//a[text()="Log In"]')
        a_login_text = a_element.text
        self.assertIn('Log In', a_login_text)

        # Em seguida, ele clica no link para ir para a página do login
        a_element.click()

        # Ao carregar a página ele ver o formulário do login
        input_username = self.browser.find_element_by_id("id_username")
        self.assertEqual(
            input_username.get_attribute('name'),
            'username'
        )
        input_password = self.browser.find_element_by_id("id_password")
        self.assertEqual(
            input_password.get_attribute('name'),
            'password'
        )

        # Carlos preenche os campos com seu username e senha
        # e clica no botão 'login'
        input_username.send_keys("carlos")
        input_password.send_keys("carlos12")

        input_submit = self.browser.find_element(By.XPATH,
                                                 '//input[@type="submit"]')
        input_submit.click()

        # Ao logar, a página carrega e Carlos ver seu nome no cabeçario
        nav_element = self.browser.find_element_by_tag_name("nav")
        self.assertIn(
            'carlos',
            nav_element.text
        )
        # No cabeçario, ele ver e clica no link 'Meus Eventos'
        a_element = self.browser.find_element(By.XPATH,
                                              '//a[text()="Meus Eventos"]')
        self.assertIn(
            "Meus Eventos",
            a_element.text
        )
        a_element.click()

        # Ao carregar a página, ele ver os links dos seus eventos cadastrados
        # e abaixo deles o botão 'Novo Evento'.
        button_novo_evento = self.browser.find_element_by_id("id_novo_evento")
        self.assertIn(
            "Novo Evento",
            button_novo_evento.text
        )
        # Ao carregar a página, Carlos ver um formulário com
        # os seguintes campos:
        # - Nome do evento;
        # - Descrição do Evento
        # - Imagem
        # - Público ou privado
        #   (Se o usuário selecionar privado, o campo Email(s)
        #   do(s) convidado(s) irá aparecer automaticamente)
        # - Hora do início do evento
        # - Local do evento
        # - Valor total

        # O usuário preenche os dados(Escolhendo a opção público
        # para o evento) e clica no botão 'Criar Evento'

        # Apos a submissão, o usuário ver a mensagem
        # "Evento criado com sucesso" e logo abaixo a visualização dos dados
        # do mesmo
if __name__ == '__main__':
    unittest.main()
